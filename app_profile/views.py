from django.shortcuts import render
from .models import Profile
# Create your views here.
user_name = 'Hepzibah Smith'
birth_date = '01 Jan'
user_gender = 'Female'
user_expertise = 'Marketing | Collector | Public Speaking'
user_description = 'Antique Expert. Experience as maketer for 10 years'
user_email = 'hello@smith.com'

def index(request):
	profile = Profile(name= user_name, birthdate=birth_date, gender=user_gender, expertise= user_expertise, description=user_description, email=user_email)
	response= {'name': profile.name, 'birthyear':profile.birthdate, 'gender': profile.gender, 'expertise': profile.expertise, 'description':profile.description, 'email': profile.email}
	return render(request, 'profile.html', response)
