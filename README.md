# Tugas 1 PPW (Kelas E - Kelompok 4)

Pengembangan​ ​Aplikasi​ ​​Web​ ​​dengan​ ​TDD,​ ​Django,​ ​Models​ ​,​ ​HTML,​ ​CSS

* * *

## Anggota Kelompok

- Agas Yanpratama (1606918396) - apps 'friends'
- Ahmad Hasan Siregar (1606892655) - apps 'status'
- Muhammad Fakhruddin Hafizh (1606875895) - apps 'stats'
- Zulia Putri Rahmadhani (1606918446) - apps 'profile' 

## Status Pipelines & Code Coverage

[![pipeline status](https://gitlab.com/a.hasan.gar/ling-in/badges/master/pipeline.svg)](https://gitlab.com/a.hasan.gar/ling-in/commits/master)
[![coverage report](https://gitlab.com/a.hasan.gar/ling-in/badges/master/coverage.svg)](https://gitlab.com/a.hasan.gar/ling-in/commits/master)

## Link Herokuapp

https://linginkuy.herokuapp.com/
