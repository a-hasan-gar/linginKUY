from django.apps import AppConfig


class FriendsConfig(AppConfig):
    name = 'app_friends'
